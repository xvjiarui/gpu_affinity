# GPU Affinity

GPU Affinity is a simple package to automatically set the CPU process affinity
to match the hardware architecture on a given platform. Setting the proper
affinity usually improves and stabilizes performance of deep learning
workloads.

This package is meant to be used for multi-process single-device workloads
(there are multiple training processes and each process is running on a single
GPU), which is typical for multi-GPU training workloads using
`torch.nn.parallel.DistributedDataParallel`.

## Features
* respects restrictions set by external environment (e.g. from `taskset` or
  from `docker run --cpuset-cpus`)
* correctly handles hyperthreading siblings
* multiple affinity mapping modes, default arguments tuned for training
  workloads on DGX machines
* automatically sets "balanced" affinity (an equal number of physical CPU cores
  is assigned to each process)

Warning: GPU-CPU affinity mapping is retrieved by `nvml` which doesn't respect
`CUDA_VISIBLE_DEVICES` env variable. Affinity mapping won't be correct if user
set `CUDA_VISIBLE_DEVICES` to limit (or reorder) GPUs visible to the
application.

## Installation

### Using pip
Install the package with `pip` directly from `gitlab-master` (requires access
to NVIDIA internal network).

```
pip install git+https://gitlab-master.nvidia.com/dl/gwe/gpu_affinity
```

### Manual installation

```
git clone ssh://git@gitlab-master.nvidia.com:12051/dl/gwe/gpu_affinity
cd gpu_affinity
pip install .
```

## Usage

Install the package and call `gpu_affinity.set_affinity(gpu_id,
nproc_per_node)` function at the beginning of the `main()` function, right
after command line arguments were parsed and before any function which performs
significant compute or creates a CUDA context.

Warning: `gpu_affinity.set_affinity()` should be called once in every process
using GPUs. Calling `gpu_affinity.set_affinity()` more than once per process
may result in errors or suboptimal setting of affinity.

## Documentation

```
def set_affinity(
    gpu_id, nproc_per_node, mode='socket_unique_continuous', balanced=True
):
    """
    The process is assigned with a proper CPU affinity which matches hardware
    architecture on a given platform. Usually it improves and stabilizes
    performance of deep learning training workloads.

    This function assumes that the workload is running in multi-process
    single-device mode (there are multiple training processes and each process
    is running on a single GPU), which is typical for multi-GPU training
    workloads using `torch.nn.parallel.DistributedDataParallel`.

    Availalbe affinity modes:
    * 'socket' - the process is assigned with all available logical CPU cores
    from the CPU socket connected to the GPU with a given id.
    * 'single' - the process is assigned with the first available logical CPU
    core from the list of all CPU cores from the CPU socket connected to the GPU
    with a given id (multiple GPUs could be assigned witht the same CPU core).
    * 'single_unique' - the process is assigned with a single unique available
    physical CPU core from the list of all CPU cores from the CPU socket
    connected to the GPU with a given id.
    * 'socket_unique_interleaved' - the process is assigned with an unique
    subset of available physical CPU cores from the CPU socket connected to a
    GPU with a given id, hyperthreading siblings are included automatically,
    cores are assigned with interleaved indexing pattern
    * 'socket_unique_continuous' - (the default) the process is assigned with an
    unique subset of available physical CPU cores from the CPU socket connected
    to a GPU with a given id, hyperthreading siblings are included
    automatatically, cores are assigned with continuous indexing pattern

    'socket_unique_continuous' is the recommended mode for deep learning
    training workloads on NVIDIA DGX machines.

    Args:
        gpu_id: integer index of a GPU
        nproc_per_node: number of processes per node
        mode: affinity mode
        balanced: assign an equal number of physical cores to each process,
            affects only 'socket_unique_interleaved' and
            'socket_unique_continuous' affinity modes

    Returns a set of logical CPU cores on which the process is eligible to run.

    WARNING: On DGX A100 only a half of CPU cores have direct access to GPUs.
    This function restricts execution only to the CPU cores directly connected
    to GPUs, so on DGX A100 it will limit the code to half of CPU cores and half
    of CPU memory bandwidth (which may be fine for many DL models).
    """
```
