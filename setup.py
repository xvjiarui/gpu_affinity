import setuptools
import pathlib

README = (pathlib.Path(__file__).parent / "README.md").read_text()

install_requires = [
    'pynvml==8.0.4',
    ]

setuptools.setup(
    name="gpu_affinity",
    version="0.0.1",
    author="",
    description="",
    long_description=README,
    long_description_content_type="text/markdown",
    url="",
    packages=["gpu_affinity"],
    install_package_data=True,
    install_requires=install_requires,
    license='Apache2',
    license_file='./LICENSE',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
