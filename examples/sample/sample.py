import argparse
import os

import gpu_affinity
import torch


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_args():
    parser = argparse.ArgumentParser(
        description='PyTorch GPU affinity sample',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '--local_rank',
        type=int,
        default=os.getenv('LOCAL_RANK', 0),
        help='Used for multi-process training.',
    )
    parser.add_argument(
        '--affinity',
        type=str,
        default='socket_unique_interleaved',
        choices=[
            'socket',
            'single',
            'single_unique',
            'socket_unique_interleaved',
            'socket_unique_continuous',
            'disabled',
        ],
        help='type of CPU affinity',
    )
    parser.add_argument(
        "--balanced",
        type=str2bool,
        nargs='?',
        const=True,
        default=True,
    )

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    print(args)
    if args.affinity != 'disabled':
        nproc_per_node = torch.cuda.device_count()
        affinity = gpu_affinity.set_affinity(
            args.local_rank, nproc_per_node, args.affinity, args.balanced
        )
        print(f'rank {args.local_rank}: core affinity: {affinity}')


if __name__ == "__main__":
    main()
